import numpy as np
import cv2

img = cv2.imread("./imgs/img2.jpeg")

img_gray = img[:,:,0]*0.114 + img[:,:,1]*0.587 + img[:,:,2]*0.299
img_gray = img_gray.astype(np.uint8)

img_gray2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

print(img.shape[0])
croped_img = img[50:200,15:150,:]

cv2.imshow("win", img)
cv2.imshow("win2", img_gray)
cv2.imshow("win3", img_gray2)
cv2.imshow("win4", croped_img)
cv2.waitKey(0)