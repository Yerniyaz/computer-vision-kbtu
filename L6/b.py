import sys
import numpy as np
import random
import matplotlib.pyplot as plt

a = -0.02
b = -0.05
c = 9

def f(x):
    return a * x**2 + b*x + c

X = np.random.randint(500, size=30)
Y = np.array([f(i) + 30 * (random.random()-0.5) for i in X])

plt.plot(X, Y, 'bo')
plt.show()