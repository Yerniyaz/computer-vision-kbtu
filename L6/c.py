import sys
import numpy as np
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def f(x):
	return 5 + 0.5*x[0] + 2*x[1]

def h(theta, x):
	return theta[0] + theta[1]*x[0] + theta[2]*x[1]

X = np.random.randint(500, size=(100, 100))
Y = np.array([f(i) + 100 * (random.random()-0.5) for i in X])

def J(theta):
	s = 0
	for i in range(len(X)):
		s += (h(theta, X[i]) - Y[i])**2
	s = s/(2*len(X))
	return s

theta = [1, 2, 3]
lr = 0.001
for i in range(1000):
	dt0 = 0
	for j in range(len(X)):
		dt0 += (h(theta, X[j]) - Y[j])
	dt0 = dt0/len(X)

	dt1 = 0
	for j in range(len(X)):
		dt1 += (h(theta, X[j]) - Y[j])*theta[1]
	dt1 = dt1/len(X)

	dt2 = 0
	for j in range(len(X)):
		dt2 += (h(theta, X[j]) - Y[j])*theta[2]
	dt2 = dt2/len(X)

	theta[0] = theta[0] - lr*dt0
	theta[1] = theta[1] - lr*dt1
	theta[2] = theta[2] - lr*dt2

print(J(theta))
# fig = plt.figure()

x = range(-100, 700, 100)
y = range(-100, 700, 100)
x, y = np.meshgrid(x, y)
z = np.array([h(theta, i) for i in zip(x, y)])

plt3d = plt.figure().gca(projection='3d')
plt3d.plot_surface(x, y, z, alpha=0.2)

# ax = Axes3D(fig)
ax = plt.gca()
# ax.hold(True)
ax.scatter(X[:,0], X[:,1], Y, c='m')

plt.show()