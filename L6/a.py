import numpy as np
import random
import matplotlib.pyplot as plt

m = 0.8
b = 5

def f(x):
    return m*x + b

def h(theta, x):
	return theta[0] + theta[1]*x

X = np.random.randint(500, size=30)
Y = np.array([f(i) + 100 * (random.random()-0.5) for i in X])

def J(theta):
	s = 0
	for i in range(len(X)):
		s += (h(theta, X[i]) - Y[i])**2
	s = s/(2*len(X))
	return s

theta = np.array([3, 0.3])
lr = 0.001
costs = []
for i in range(100):
	dt0 = 0
	for j in range(len(X)):
		dt0 += (theta[0] + theta[1]*X[j] - Y[j])
	dt0 = dt0/len(X)

	dt1 = 0
	for j in range(len(X)):
		dt1 += (theta[0] + theta[1]*X[j] - Y[j])*theta[1]
	dt1 = dt1/len(X)

	theta[0] = theta[0] - lr*dt0
	theta[1] = theta[1] - lr*dt1

	costs.append(J(theta))

print(theta)
print(J(theta))

plt.plot(X, Y, 'ro')

x = range(500)
y = [h(theta, i) for i in x]
plt.plot(x, y)
# plt.plot(costs)

plt.show()