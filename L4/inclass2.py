import cv2
import numpy as np
import glob

def gerCorners(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur = cv2.GaussianBlur(img, (11,11), 0)
    blur = cv2.medianBlur(img, 13)

    edges = cv2.Canny(blur, 70, 150)

    lines = cv2.HoughLines(edges, 1, np.pi/180, 100)
    lines2 = []
    for it in lines:
        rho, theta = it[0]

        bool_ = True
        for rho2, theta2 in lines2:
            dif_ang = min(abs(theta2-theta), abs(3.14-abs(theta2-theta)))
            if abs(abs(rho2)-abs(rho)) < img.shape[0]/20 and dif_ang < 3.14/4:
                bool_ = False
                break
        if bool_:
            lines2.append((rho, theta))

    lines_ver = []
    lines_hor = []
    for rho, theta in lines2:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        if theta < 0.5  or theta > 2.6: 
            lines_hor.append(((x1, y1), (x2, y2)))
        else:
            lines_ver.append(((x1, y1), (x2, y2)))

    # for (x1, y1), (x2, y2) in lines_hor:
    #     cv2.line(img, (x1,y1), (x2,y2), (0, 255,0), 2)

    # for (x1, y1), (x2, y2) in lines_ver:
    #     cv2.line(img, (x1,y1), (x2,y2), (255, 0,0), 2)

    points = []
    for (x1, y1), (x2, y2) in lines_hor:
        for (x3, y3), (x4, y4) in lines_ver:
            if (x2-x1) == 0:
                a = (y2-y1)/0.001
            else:
                a = (y2-y1)/(x2-x1)
            b = (y4-y3)/(x4-x3)
            x = (x1*a - x3*b - y1 + y3)/(a-b)
            y = a*x - a*x1 + y1

            # cv2.circle(img, (int(x), int(y)), 4, (0,0,255), -1)
            points.append((int(x), int(y)))

    up_points = []
    down_points = []
    for p1 in points:
        count = 0
        for p2 in points:
            if p1[1] > p2[1]:
                count += 1
        if count >= 2:
            down_points.append(p1)
        else:
            up_points.append(p1)

    if up_points[0][0] > up_points[1][0]:
        p1 = up_points[1]
        p2 = up_points[0]
    else:
        p1 = up_points[0]
        p2 = up_points[1]

    if down_points[0][0] > down_points[1][0]:
        p3 = down_points[1]
        p4 = down_points[0]
    else:
        p3 = down_points[0]
        p4 = down_points[1]


    return [p1, p2, p3,p4]

ls = glob.glob("./data/*.jpg")
for path in ls:
    img = cv2.imread(path)
    img_origin = img.copy()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (7,7), 0)
    img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)

    points = gerCorners(img)
    points1 = np.array(points, dtype=np.float32)*2

    points2 = [[0,0], [500,0], [0,700], [500, 700]]
    points2 = np.array(points2, dtype=np.float32)

    M = cv2.getPerspectiveTransform(points1, points2)
    pers_img = cv2.warpPerspective(gray, M, (500, 700))
    pers_img2 = cv2.warpPerspective(img_origin, M, (500, 700))

    for p in points:
        cv2.circle(img, p, 6, (0,255,0), -1)


    thres = cv2.adaptiveThreshold(pers_img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            cv2.THRESH_BINARY,11,2)

    cv2.imshow("thres1", thres)
    kernel = np.ones((3,3), np.uint8)
    thres = cv2.erode(thres, kernel, iterations = 1)
    
    kernel = np.ones((1,3), np.uint8)
    thres = cv2.erode(thres, kernel, iterations = 1)

    im, contours, _ = cv2.findContours(thres.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        if cv2.contourArea(cnt) > 100:
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(pers_img2, (x, y), (x+w, y+h), (0, 255, 0), 1)


    cv2.imshow("img", img)
    cv2.imshow("Pers", pers_img2)
    cv2.imshow("thres", thres)
    # cv2.imshow("dilation", dilation)
    k = cv2.waitKey(0)
    if k == 1048603:
        break
