import cv2
import numpy as np

img = cv2.imread("./imgs/img3.jpeg")
img = cv2.resize(img, (0,0), fx=0.25, fy=0.25)

# points1 = np.array([[0, 0], [500, 0], [0, 500]], dtype=np.float32)
# points2 = np.array([[0, 0], [400, 100], [20, 600]], dtype=np.float32)
# M = cv2.getAffineTransform(points1, points2)

M = cv2.getRotationMatrix2D((img.shape[1]//2, img.shape[0]//2), 45, 1)

out_img = cv2.warpAffine(img, M, (1000, 1000))

cv2.imshow("img", img)
cv2.imshow("affine", out_img)
cv2.waitKey(0)
