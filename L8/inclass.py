import pandas as pd
import numpy as np
import cv2

df = pd.read_csv('./mnist_train.csv')

for i in range(len(df)):
	img = df.iloc[i][1:].values.reshape(28, 28)
	img = img.astype(np.uint8)

	label = df.iloc[i][0]

	cv2.imshow("img", img)
	print(label)
	k = cv2.waitKey(0)
