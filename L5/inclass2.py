import cv2
import numpy as np

feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 15,
                       blockSize = 7 )

lk_params = dict( winSize  = (15,15),
                   maxLevel = 2,
                   criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))


cap = cv2.VideoCapture("./videos/highway.mp4")
# cap = cv2.VideoCapture(0)
back_sub = cv2.createBackgroundSubtractorMOG2(detectShadows=False)

p0 = None
count = 0
while(True):
    count+=1
    ret, frame = cap.read()
    frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5)
    vis = frame.copy()
    if not ret:
        break
    
    blur = cv2.GaussianBlur(frame, (15,15), 0)
    mask = back_sub.apply(blur)

    kernel = np.ones((3,3), np.uint8)
    mask = cv2.erode(mask, kernel, iterations = 2)
    mask = cv2.dilate(mask, kernel, iterations = 5)

    good_points = None
    if p0 is not None:
        p1, st, err = cv2.calcOpticalFlowPyrLK(prev_frame, frame, p0, None, **lk_params)
        good_points = p1[st==1]

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    if count > 15:
        if good_points is not None:
            # print(good_points.shape)
            for i in range(good_points.shape[0]):
                cv2.circle(mask,(good_points[i][0], good_points[i][1]), 15, 0,-1)

        p0 = cv2.goodFeaturesToTrack(gray,
                    mask=mask,
                    **feature_params)

        if good_points is not None:
            good_points = good_points.reshape(-1, 1, 2)
            if p0 is not None:
                p0 = np.concatenate([p0, good_points], axis=0)
            else:
                p0 = good_points
            # print(good_points.shape, p0.shape)
    
        if p0 is not None:
            for i in range(p0.shape[0]):
                cv2.circle(vis,(p0[i][0][0],p0[i][0][1]), 3, (0,0,255),-1)


    cv2.imshow('frame', vis)
    cv2.imshow('mask', mask)

    prev_frame = frame

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break


cv2.destroyAllWindows()
cap.release()
