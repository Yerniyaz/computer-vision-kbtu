import cv2
import numpy as np

# img = cv2.imread("./imgs/img1.jpg")
# img = cv2.resize(img, (0,0), fx=0.25, fy=0.25)

# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# ret, thres = cv2.threshold(gray, 110, 255, cv2.THRESH_BINARY)

# im, contours, _ = cv2.findContours(thres.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# for cnt in contours:
# 	if cv2.contourArea(cnt) > 1000:
# 		x, y, w, h = cv2.boundingRect(cnt)
# 		cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

# 		rect = cv2.minAreaRect(cnt)
# 		box = cv2.boxPoints(rect)
# 		box = np.int64(box)
# 		cv2.drawContours(img, [box], -1, (0, 0, 255), 2)

# cv2.imshow("win", img)

#####################################################

# gray = cv2.imread("./imgs/img4.jpg", 0)
# ret, thres = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

# kernel = np.ones((3,3), np.uint8)
# closing = cv2.morphologyEx(thres, cv2.MORPH_CLOSE, kernel)
# # opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=2)

# erosion = cv2.erode(closing, kernel, iterations = 5)
# dilation = cv2.dilate(erosion, kernel, iterations = 5)


# cv2.imshow("thres", thres)
# cv2.imshow("closing", closing)
# # cv2.imshow("opening", opening)
# cv2.imshow("dilation", dilation)
# cv2.imshow("erosion", erosion)

#####################################################

# gray = cv2.imread("./imgs/img5.jpeg", 0)

# kernel = np.array([
# 	[1, 0, -1],
# 	[2, 0, -2],
# 	[1, 0, -1]
# 	])

# kernel = np.array([
# 	[0, 1, 0],
# 	[1, -4, 1],
# 	[0, 1, 0]
# 	])

# kernel = np.array([
# 	[1, 1, 1],
# 	[1, 1, 1],
# 	[1, 1, 1]
# 	])/9.

# img = cv2.filter2D(gray, ddepth=cv2.CV_64F, kernel=kernel)
# img = abs(img)

# img = (img - np.min(img))/(np.max(img)-np.min(img)) * 255

# img = img.astype(np.uint8)

# print(gray.shape)
# print(img.shape)

###############################################

img = cv2.imread("./imgs/img5.jpeg")
canny = cv2.Canny(img, 50, 180)

# cv2.imshow("gray", gray)
# cv2.imshow("img", img)
cv2.imshow("canny", canny)
cv2.waitKey(0)

