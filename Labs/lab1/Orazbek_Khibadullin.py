import cv2
import numpy as np
import copy
import glob

ls = glob.glob("./data/*.jpg")
for path in ls:
    img = cv2.imread(path)
    img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
    img_orig = copy.copy(img)
    grayimg = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)    
    grayimg =  cv2.GaussianBlur(grayimg, (11,11), 0);
    grayimg =  cv2.GaussianBlur(grayimg, (11,11), 0);
    corners = cv2.goodFeaturesToTrack(grayimg,4,0.005,1)
    corners = np.float32(corners)
     
    for item in corners:
        x,y = item[0]
        cv2.circle(img,(x,y),5,255,-1)
                
    cv2.imshow("win",img)
    print(cv2.waitKey(0))
    cv2.waitKey(0)
            
# goodFeaturesToTrack does not find corners in some cases
