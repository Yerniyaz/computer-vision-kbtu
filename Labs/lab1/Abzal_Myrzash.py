import cv2
import numpy as np
import glob
def intersection(x11, y11, x12, y12, x21, y21, x22, y22):
    o1 = np.array((x11, y11))
    p1 = (x12, y12)
    o2 = np.array((x21, y21))
    p2 = (x22, y22)
    x = o2 - o1;
    d1 = p1 - o1;
    d2 = p2 - o2;

    cross = d1[0]*d2[1] - d1[1]*d2[0];
    if abs(cross) < 1e-8:
        return (-1, -1)

    t1 = (x[0] * d2[1] - x[1] * d2[0])/cross;
    r = o1 + d1 * t1;
    return r
  
ls = glob.glob("./data/*.jpg")
for path in ls:
    img = cv2.imread(path)
    img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)


    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (7,7), 0)
    edges = cv2.Canny(blur, 200, 150)
    lines = cv2.HoughLines(edges, 1, np.pi/180, 100)
    if lines is None:
        continue
    o_arr = []
    p_arr = []
    for it in lines:
        rho, theta = it[0]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        
        cv2.line(img, (x1,y1), (x2,y2), (0,0,255), 2)
        o_arr.append((x1,y1))
        p_arr.append((x2,y2))

    for i in range(len(o_arr)):
        for j in range(i+1, len(o_arr)):
            r = intersection(o_arr[i][0], o_arr[i][1], p_arr[i][0], p_arr[i][1], o_arr[j][0], o_arr[j][1], p_arr[j][0], p_arr[j][1])
            if r[0] != -1 and r[1] != -1:
                cv2.circle(img, (int(r[0]), int(r[1])), 5, (255, 0, 0), 2)

    cv2.imshow("img", img)
    k = cv2.waitKey(0)
    if k == 27:
        break
