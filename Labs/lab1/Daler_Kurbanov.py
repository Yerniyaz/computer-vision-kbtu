import cv2
import numpy as np
import glob

def det (A, B, A2, B2):
    return A * B2 - B * A2;


"""def intersect(A, B, C, A2, B2, C2):
    zn = det (A, B, A2, B2);
    
    x = - det (C, B, C2, B2) / zn;
    y = - det (A, C, A2, C2) / zn;
    return x, y
"""

def intersection(line1, line2):
    
    rho1, theta1 = line1
    rho2, theta2 = line2    
    A = np.array([
        [np.cos(theta1), np.sin(theta1)],
        [np.cos(theta2), np.sin(theta2)]
    ])
    b = np.array([[rho1], [rho2]])
    x0, y0 = np.linalg.solve(A, b)
    #print(x0, y0, "!!!!")
    x0, y0 = int(np.round(x0)), int(np.round(y0))
    return x0, y0


ls = glob.glob("./data/*.jpg")
for path in ls:
    img = cv2.imread(path)
    #img = cv2.resize(img, (960, 540))
    # place for your code
    

    cv2.imshow("img", img)    
    
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    gray = cv2.medianBlur(gray, 13)
    
    #gray = cv2.GaussianBlur(gray, (9, 9), 0)

    #cv2.imshow("Blur", gray)
    
    canny = cv2.Canny(gray, 70, 150)

    cv2.imshow("Edges", canny)
    
    lines = cv2.HoughLines(canny, 1, np.pi/180, 120)
    
    resultingLines = []
    
    verticalLines = []
    
    horizontalLines = []
    
    for line in lines:
        dist, angle = line[0]        
        x = np.cos(angle)
        y = np.sin(angle)
        
        x0 = x * dist
        y0 = y * dist
        
        x1 = int(x0 - 5000 * y)
        y1 = int(y0 + 5000 * x)
        x2 = int(x0 + 5000 * y)
        y2 = int(y0 - 5000 * x)    
        
        #cv2.line(img,(x1 ,y1),(x2, y2),(0 ,0, 255), 2)
        
        resultingLines.append(((x1, y1), (x2, y2)))
        
        #print (angle / np.pi * 180)
        
        if angle / np.pi * 180 >= 135 or angle / np.pi * 180 <= 45:
            horizontalLines.append((dist, angle))
            #horizontalLines.append(((x, y), (x1, y1), (x2, y2)))
            #horizontalLines.append((x0, y0, x1, y1, x2, y2))
            cv2.line(img,(x1 ,y1),(x2, y2),(0 ,0, 255), 2)
        else:
            verticalLines.append((dist, angle))
            #verticalLines.append(((x, y), (x1, y1), (x2, y2)))
            #verticalLines.append((x0, y0, x1, y1, x2, y2))
            cv2.line(img,(x1 ,y1),(x2, y2),(0 ,255, 0), 2)
            
        #print (x1, y1)
        #print (x2, y2)
        #print ("##################")
    
    #cv2.imshow("Result", img) 
    
    for hl in horizontalLines:
        for vl in verticalLines:
            #x1, y1, x11, y11, x12, y12 = vl
            #x2, y2, x21, y21, x22, y22 = hl
            pointX, pointY = intersection(hl, vl)
            
            #print (pointX, pointY)       
            
            cv2.circle(img,(int(pointX), int(pointY)),100,(0,0,255),1)
            """
            print(x1, y1, x11, y11, x12, y12)
            print ("DEBUG")
            print (x11 * x1 + y1, y11)
            print (x12 * x1 + y1, y12)
            print(x2, y2, x21, y21, x22, y22)
            print ("NVM")
            print (x11 * x1 + y1)
            print ("NVM")
            if np.absolute(x11 * x1 + y1) > 1000 or np.absolute(x12 * x1 + y1) > 1000:
                print("MATE?!")            
                cv2.line(img,(x11 ,y11),(x12, y12),(255 ,255, 0), 5)
                
            else:
                cv2.line(img,(x11 ,y11),(x12, y12),(0 ,255, 0), 2)
            cv2.line(img,(x21 ,y21),(x22, y22),(0 ,0, 255), 2)   
            #img = cv2.resize(img, (960, 540))
            
            
            A1 = -x1
            B1 = 1
            C1 = -y1
            
            A2 = -x2
            B2 = 1
            C2 = -y2
            
            if det(A1, B1, A2, B2) < np.finfo(float).eps:
                continue
                    
            print ("||||||||||||||||")
            print (det(A1, B1, A2, B2))
            pointX, pointY = intersect(A1, B1, C1, A2, B2, C2)
            print (A1, B1, C1, A2, B2, C2)
            print("POINT")
            print(pointX, pointY)
            
            cv2.circle(img,(int(pointX), int(pointY)),100,(0,0,255),1)
            #img = cv2.resize(img, (960, 540))
            #cv2.imshow("CIRCLES", img) 
            print ("----------")
            #cv2.waitKey(0)"""
    cv2.imshow("CIRCLES", cv2.resize(img, (0,0), fx=0.5, fy=0.5))             
    k = cv2.waitKey(0)
    
         
    if k == 27:
        cv2.destroyAllWindows()
        break    
