import cv2
import numpy as np

# path = "/Users/User/Pictures/data/7.jpg"
path = "./data/1.jpg"
img = cv2.imread(path)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# (ret, thresh) = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
edge = cv2.Canny(gray, 100, 200) # you should pass gray image not thres
(_ ,cnts, _) = cv2.findContours(edge.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

total = 2
for c in cnts:
    epsilon = 0.08 * cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, epsilon, True)

    if cv2.contourArea(approx) < 100: # you should add some thing like this
    	continue
    cv2.drawContours(img, [approx], -1, (0, 255, 0), 2)
    total += 1

cv2.imshow("Output", cv2.resize(img, (0,0), fx=0.5, fy=0.5))
cv2.waitKey(0)
