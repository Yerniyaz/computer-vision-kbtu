import cv2
import numpy as np
import glob

ls = glob.glob("./data/*.jpg")
for path in ls:
    img = cv2.imread(path)
    img = cv2.medianBlur(img,5)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,20,220) # wrong thresholds
#    cv2.imshow("gray",gray)    
#    cv2.imshow("edges", edges)
    lines = cv2.HoughLines(edges,1,np.pi/180,210)
    for it in lines:
        rho,theta=it[0]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        cv2.imshow('img',img)
    cv2.waitKey(0)==27
    #break
    
	# place for your code


#	cv2.imshow("img", img)
