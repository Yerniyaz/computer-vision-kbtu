import cv2
import numpy as np
import glob

ls = glob.glob("./data/*.jpg")
for path in ls:
	img = cv2.imread(path)

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	gray = np.float32(gray)
	gray = cv2.blur(gray,(5,5))

	dst = cv2.cornerHarris(gray,5,21,0.04)
	ret, dst = cv2.threshold(dst,0.1*dst.max(),255,0)
	dst = np.uint8(dst)
	ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.01)
	corners = cv2.cornerSubPix(gray,np.float32(centroids),(15,15),(-1,-1),criteria)
	img[dst>0.9*dst.max()]=[0,255,0]

	

	cv2.imshow("img", cv2.resize(img, (0,0), fx=0.5, fy=0.5))
	if cv2.waitKey(0) == 27:
		break
# does not work for some images