import cv2
import numpy as np
import glob

ls = glob.glob("./data/*.jpg")

for path in ls:
	img = cv2.imread(path)
	nimg = cv2.resize(img, (0,0), fx=0.5, fy=0.5) 
	gray = cv2.cvtColor(nimg,cv2.COLOR_BGR2GRAY)
	blur = cv2.medianBlur(gray,11) #s malenkim blurom ne stavyatsya tochki
	#blur = cv2.blur(gray,(5,5))
	#blur = cv2.GaussianBlur(gray,(9,9),0)
	#ret, thresh1 = cv2.threshold(gray, 160, 255, cv2.THRESH_BINARY)

	corners = cv2.goodFeaturesToTrack(blur,4,0.01,200)
	corners = np.int0(corners)
	
	for i in corners:
		x,y = i.ravel()
		cv2.circle(nimg,(x,y),3,(0,0,255),-1)

	cv2.imshow("img", nimg)
	k = cv2.waitKey(0)
	print(k)
	if k == 32: #spacebar
		break
