import cv2
import numpy as np
import glob
np.set_printoptions(formatter={"float_kind": lambda x: "%g" % x})
ls = glob.glob("./data/*.jpg")
count = 0
for path in ls:
    img = cv2.imread(path)
    img = cv2.resize(img, (0,0), fx=0.7, fy=0.7)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
    kernel = np.ones((7,7),np.float32)/25 #shuld be 49
    blur = cv2.filter2D(gray,-1,kernel)
    #cv2.imshow("blur", blur)
    
    edges = cv2.Canny(blur, 50, 150,apertureSize = 3)
    #cv2.imshow("edges", edges)
    
    lines = cv2.HoughLines(edges,1,np.pi/180,100)
    print(lines) # sometimes lines is none
    if lines is None:
        continue
    vertical = np.zeros(shape = (2, 20))
    vs = 0
    hs = 0
    horizontal = np.zeros(shape = (2, 20))
    for it in lines:
        rho = it[0][0]
        theta = it[0][1]
        if abs(theta - 3.14) < 0.4:
            horizontal[0][hs] = rho
            horizontal[1][hs] = theta
            hs += 1
        if abs(theta) < 0.4:
            horizontal[0][hs] = rho
            horizontal[1][hs] = theta
            hs += 1
        if abs(theta - 1.57) < 0.4:
            vertical[0][vs] = rho
            vertical[1][vs] = theta
            vs += 1
        
    for i in range(vs):
        for j in range(hs):
            rho0 = vertical[0][i]
            theta0 = vertical[1][i]
            a = np.cos(theta0)
            b = np.sin(theta0)
            x0 = a*rho0
            y0 = b*rho0
            
            rho1 = horizontal[0][j]
            theta1 = horizontal[1][j]
            a = np.cos(theta1)
            b = np.sin(theta1)
            x1 = a*rho1
            y1 = b*rho1    
            theta0 = theta0 + 1.57
            theta1 = theta1 + 1.57
            x = (np.tan(theta1)*x1 - np.tan(theta0)*x0 - y1 + y0)/(np.tan(theta1) - np.tan(theta0))
            y = np.tan(theta1)*(x - x1) + y1
            #print(x, y)
            cv2.circle(img,(int(x), int(y)),3,(255,0,255),-1)
    count += 1
    cv2.imshow("img", cv2.resize(img, (0,0), fx=0.5, fy=0.5))
    cv2.waitKey(0)
    if count == 27:
        break

# sometimes lines is none
# you found several points for one corner
