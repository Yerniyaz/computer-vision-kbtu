import cv2
import numpy as np

# img = cv2.imread("./imgs/img5.jpeg")
# img2 = img.copy()
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# edges = cv2.Canny(gray, 50, 150)

# lines = cv2.HoughLines(edges, 1, np.pi/180, 110)
# for it in lines:
#     rho, theta = it[0]
#     a = np.cos(theta)
#     b = np.sin(theta)
#     x0 = a*rho
#     y0 = b*rho
#     x1 = int(x0 + 1000*(-b))
#     y1 = int(y0 + 1000*(a))
#     x2 = int(x0 - 1000*(-b))
#     y2 = int(y0 - 1000*(a))

#     cv2.line(img, (x1,y1), (x2,y2), (0,0,255), 1)

# lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, 50, 5)
# for it in lines:
#     x1, y1, x2, y2 = it[0]
#     cv2.line(img2, (x1,y1), (x2,y2), (0,255,0), 1)

# cv2.imshow("win", img)
# cv2.imshow("win2", img2)
# cv2.waitKey(0)

########################################################

# # img = cv2.imread("./imgs/img1.jpeg")
# img = cv2.imread("./imgs/img2.png")
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# gray = cv2.GaussianBlur(gray, (9, 9), 0);
# edges = cv2.Canny(gray, 50, 100)

# # circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20,
#                            # param1=100, param2=30, minRadius=10, maxRadius=40)

# circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 100,
#                            param1=100, param2=50, minRadius=30, maxRadius=80)

# # circles = np.uint16(np.around(circles))
# for i in circles[0,:]:
#     cv2.circle(img, (i[0],i[1]), i[2], (0,255,0), 2)
#     cv2.circle(img, (i[0],i[1]), 2, (0,0,255), -1)

# cv2.imshow("win", img)
# cv2.imshow("gray", gray)
# cv2.imshow("edges", edges)
# cv2.waitKey(0)

################################################################


# img = cv2.imread("./imgs/6.jpg")
# img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# gray = cv2.GaussianBlur(gray, (9, 9), 0);
# edges = cv2.Canny(gray, 50, 150)

# lines = cv2.HoughLines(edges, 1, np.pi/180, 100)
# for it in lines:
#     rho, theta = it[0]
#     a = np.cos(theta)
#     b = np.sin(theta)
#     x0 = a*rho
#     y0 = b*rho
#     x1 = int(x0 + 1000*(-b))
#     y1 = int(y0 + 1000*(a))
#     x2 = int(x0 - 1000*(-b))
#     y2 = int(y0 - 1000*(a))

#     cv2.line(img, (x1,y1), (x2,y2), (0,0,255), 1)


# cv2.imshow("win", img)
# cv2.imshow("edges", edges)
# cv2.imshow("gray", gray)
# cv2.waitKey(0)
