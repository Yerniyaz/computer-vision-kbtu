import cv2
import numpy as np

# img = cv2.imread('./imgs/img5.jpeg')
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# edges = cv2.Canny(gray, 50, 150)

# lines = cv2.HoughLines(edges, 1, np.pi/180, 120)
# print(lines.shape)
# for it in lines:
#     rho, theta = it[0]
#     a = np.cos(theta)
#     b = np.sin(theta)
#     x0 = a*rho
#     y0 = b*rho
#     x1 = int(x0 + 1000*(-b))
#     y1 = int(y0 + 1000*(a))
#     x2 = int(x0 - 1000*(-b))
#     y2 = int(y0 - 1000*(a))

#     cv2.line(img, (x1,y1), (x2,y2), (0,0,255), 2)

# cv2.imshow("win", img)
# cv2.imshow("edges", edges)
# cv2.waitKey(0)


img = cv2.imread('./imgs/img2.png')
img = cv2.GaussianBlur(img, (15,15), 0)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 50, 100)

circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20,
        param1=100, param2=30, minRadius=10,maxRadius=60)
for i in circles[0,:]:
    cv2.circle(img,(i[0],i[1]), i[2], (0,255,0), 2)
    cv2.circle(img,(i[0],i[1]), 2, (0,0,255), 3)

cv2.imshow("win", img)
cv2.imshow("edges", edges)
cv2.waitKey(0)
