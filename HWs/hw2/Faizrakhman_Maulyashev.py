import cv2

img = cv2.imread("./data_test/138.jpg") 
cv2.imshow("Real", img)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
ret2, th2 = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
threshed = cv2.morphologyEx(th2, cv2.MORPH_CLOSE, kernel)

imgContours, Contours, Hierarchy = cv2.findContours(threshed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
for contour in Contours:

    if cv2.contourArea(contour) > 400:
        [X, Y, W, H] = cv2.boundingRect(contour)
        cv2.rectangle(img, (X, Y), (X + W, Y + H), (0,255,0), 1)
        
cv2.imshow("Contour", img)
cv2.waitKey(0)


