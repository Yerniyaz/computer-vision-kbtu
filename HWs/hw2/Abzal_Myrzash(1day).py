import cv2
import numpy as np
import glob

def segmentation(img):
    height = img.shape[0]
    width = img.shape[1]
    X = [0]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    thres1 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    
    kernel = np.ones((2,3), np.uint8)
    for i in range(1):
        thres = cv2.dilate(thres1, kernel, iterations = 1)
        thres = cv2.erode(thres, kernel, iterations = 1)
        
    for x in range(width):
        cnt = 0
        for y in range(int(height*0.05), int(height*0.95)):
            color = thres[y,x]
            left_color   =  0  if x == 0         else  thres[y,x-1]
            right_color  =  0  if x == width-1   else  thres[y,x+1]
            up_color     =  0  if y == 0         else  thres[y-1,x]
            bottom_color =  0  if y == height-1  else  thres[y+1,x]
            
            if color == 0:
                if left_color == 0:
                    cnt += 1
                if right_color == 0:
                    cnt += 1
                if up_color == 0:
                    cnt += 1
                if bottom_color == 0:
                    cnt += 1
        if cnt < 5:
            X.append(int(x))
                
    X.append(width-1)
    cv2.imshow("thres1", thres1)
    cv2.imshow("thres", thres)
    return X

ls = glob.glob("./data_test/*.jpg")
for path in ls:
    img = cv2.imread(path)
    cv2.imshow("orig", img)
    X = segmentation(img)
    for x in X:
        cv2.line(img, (x, 0), (x, 1000), (0,255,0), 1)


    cv2.imshow("img", img)
    k = cv2.waitKey(0)
    if k == 27:
        break
    
cv2.destroyAllWindows()
    
