import numpy as np
import cv2 as cv
import glob

ls = glob.glob("./data_test/*.jpg")
for path in ls:
    img = cv.imread(path)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.GaussianBlur(gray, (3, 3), 0)
    thres = cv.adaptiveThreshold(gray,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv.THRESH_BINARY_INV,13,2)
    kernel = np.ones((3,3), np.uint8)
    thres = cv.erode(thres, kernel, iterations = 1)
    thres = cv.dilate(thres, kernel, iterations = 1)
    thres = cv.erode(thres, kernel, iterations = 1)
    thres = cv.dilate(thres, kernel, iterations = 1)
    thres = cv.erode(thres, kernel, iterations = 1)
    thres = cv.dilate(thres, kernel, iterations = 1)
    height = np.size(img, 0)
    kernel = np.ones((3,1), np.uint8)
    thres = cv.erode(thres, kernel, iterations = 1)
    thres = cv.dilate(thres, kernel, iterations = 1)
    thres = cv.erode(thres, kernel, iterations = 1)
    thres = cv.dilate(thres, kernel, iterations = 1)
    im, contours, _ = cv.findContours(thres.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    segment = []
    for cnt in contours:
        if cv.contourArea(cnt) > 60:
            x, y, w, h = cv.boundingRect(cnt)
            segment.append(x)
    segment.sort(reverse = False)
    segmentation = []
    index = 0
    p = len(segment)
    print(p)
    for i in segment:
        if segment[index + 1] - segment[index] >= 12:
            segmentation.append(segment[index + 1])
        index += 1
        if index == p - 1:
            break
    for i in segmentation:
        print(i)
        cv.line(img, (i, 0), (i, height), (0, 255, 0), 1)
    cv.imshow('img', img)
    cv.imshow('thres', thres)
    cv.waitKey(0)