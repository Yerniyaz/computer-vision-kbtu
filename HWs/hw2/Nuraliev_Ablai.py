import cv2
import numpy as np
import glob

ls=glob.glob('./data_test/*.jpg')

for path in ls:
	img = cv2.imread(path)
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(gray,(3,3),0)
	thres = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,2)

	kernel = np.ones((3,1), np.uint8)
	thres = cv2.erode(thres, kernel, iterations = 1)
	

	

	hi=np.size(img,0)




	_, contours, num = cv2.findContours(thres.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	for cnt in contours:
		if cv2.contourArea(cnt) > 50:
			x, y, w, h = cv2.boundingRect(cnt)
			#cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
			cv2.line(img,(x,0),(x,hi),(0,255,0),1)
			

	cv2.imshow("thres1", img)
	cv2.imshow("thres12", thres)

	cv2.waitKey(0)