import cv2
import numpy as np
import glob
np.set_printoptions(formatter={"float_kind": lambda x: "%g" % x})

def segmetation(img):
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    h, w = gray.shape
    sum = 0
    for i in range(w):
        for j in range(h):
            sum += gray[j][i]
    ave = sum / (w * h)
    
    retval,mask_img = cv2.threshold(gray, ave-8, 255, cv2.THRESH_BINARY)
    cv2.imshow("mask img", mask_img)
    
    th = int(h*0.08)
    lines = np.array([])
    lines = np.append(lines, 0)
    for i in range(1, w - 1):
        sum = 0
        for j in range(th,h-th):
            if mask_img[j][i] == 255:
                sum += 1
        if sum > 0.9*(h-th*2):
            lines = np.append(lines, i)
    lines = np.append(lines, w - 1)
    
    newL = np.array([])
    newL = np.append(newL, 0)
    i = 0
    while i < lines.size - 1:
        count = 1
        while i + 1 < lines.size and lines[i] == lines[i + 1] - 1:
            count += 1
            i += 1
        x = int(lines[i - int(count/2)])
        if x - newL[-1] > w * 0.05:
            newL = np.append(newL, x)
        i +=  1
        
    pLines = np.array([])
    for i in range(lines.size - 1):
        if lines[i + 1] - lines[i] > w*0.15:
            l = (lines[i + 1] + lines[i]) / 2
            l = int(l)
            pLines = np.append(pLines, l)
            newL = np.append(newL, l)
    
    np.sort(newL)
    return newL

ls = glob.glob("./data_test/*.jpg")
count = 0
for path in ls:
    img = cv2.imread(path)
    
    h, _, _ = img.shape
    lines = segmetation(img)
    
    for x in lines:
        x = int(x)
        cv2.line(img,(x,0),(x,h),(0,255,0),2)
    
    count += 1
    cv2.imshow("img", img)
    cv2.waitKey(0)
    if count == 27:
        break
cv2.destroyAllWindows()