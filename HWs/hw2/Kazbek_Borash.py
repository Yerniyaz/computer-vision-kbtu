import numpy as np
import cv2
import glob

def segmentation(img):
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(gray,3)
    canny = cv2.Canny(gray, 50, 150)
    retval, edges = cv2.threshold(gray, 1,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU) 
    # edges = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 17, 5)
    th3 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 5, 5)
    cv2.imshow('edges',edges)
    # cv2.imshow('canny',canny)
    # cv2.imshow('th3', th3)
    # cv2.imshow('th', th)
    ans = []

    for i in range(len(edges[0])):
        ans.append(0)

    for i in range(len(edges)):
        for j in range(len(edges[0])):
            cnt = 0
            for a in range(-4,4):
                for b in range(-4,4):
                    if i + a < 0   or j + b < 0 or i + a >= len(edges)  or j + b >= len(edges[i]):
                        cnt+=0
                    else:
                        cnt +=  edges[i][j]
            if cnt/81 <= 128:
                ans[j] += 1


    print(ans)
    return ans

ls = glob.glob("./data_test/*.jpg")
for path in ls:
    img = cv2.imread(path)
    res = segmentation(img)
    mx = len(img[0])

    for i in range(len(res)):
        if res[i] < 6:
            cv2.line(img, (i,0), (i, mx), (255,0,0), 1)

    cv2.imshow('img',img)
    cv2.waitKey(0)
cv2.destroyAllWindows()
